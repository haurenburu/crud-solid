import { Router } from 'express';
import MemoryDAO from '../database/MemoryEmployeeDAO';
import EmployeeRepo from '../repositories/EmployeeRepository';
import { uuid } from 'uuidv4';
import cltEmployee from '../models/CLTEmployee';

const router = Router();
const repository = new EmployeeRepo(new MemoryDAO());

router.get('/', (req, res) => {
    return res.json(repository.findAll());
})

router.post('/clt', (req, res) => {
    const {
        name,
        cpf,
        baseSalary
    } = req.body;

    const emplo = new cltEmployee(name, cpf, baseSalary)

    if(repository.insert(emplo)) {
        return res.status(201).json("created");
    }
    return res.status(503).json("something wrong");
})

export default router;