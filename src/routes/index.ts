import { app } from '../app';

import cltEmployeerRoutes from './employee';

app.use('/', cltEmployeerRoutes);

export default app;