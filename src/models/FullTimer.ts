import Employee from "./Employee";

export default interface FullTimer extends Employee {
    calcTaxes(): number
}