import { uuid } from "uuidv4";
import FullTimer from "./FullTimer";

export default class cltEmployee implements FullTimer {
    id: string;
    name: string;
    cpf: string;
    baseSalary: number;

    constructor(name: string, cpf: string, baseSalary: number) {
        this.id = uuid();
        this.name = name;
        this.cpf = cpf;
        this.baseSalary = baseSalary;
    }
    calcTaxes(): number {
        return this.baseSalary * 0.3;
    }

    payment() : number {
        return this.baseSalary - this.calcTaxes();
    }
}