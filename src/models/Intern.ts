import { uuid } from "uuidv4";
import Employee from "./Employee";

export default class Intern implements Employee {
    id: string;
    name: string;
    cpf: string;
    baseSalary: number;
    assistance = 200.00;

    constructor(name: string, cpf: string, baseSalary: number) {
        this.id = uuid();
        this.name = name;
        this.cpf = cpf;
        this.baseSalary = baseSalary
    }

    payment(): number {
        return this.baseSalary + this.assistance;
    }
    
}