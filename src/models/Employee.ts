export default interface Employee {
    id: string
    name: string
    cpf: string
    baseSalary: number
    payment(): number
}