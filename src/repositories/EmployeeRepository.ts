// import EmployeeDAO from "../database/EmployeeDAO";
import EmployeeDAO from "../database/MySQLDAO";
import Employee from "../models/Employee";

export default class EmployeeRepository {
    private dao: EmployeeDAO;

    constructor(employeeDao: EmployeeDAO) {
        this.dao = employeeDao;
    }

    insert(employee: Employee): boolean {
        return this.dao.insert(employee);
    }

    findbyId(id: string): Employee | null {
        return this.dao.findbyId(id);
    }

    findAll(): Employee[] | null {
        return this.dao.findAll();
    }

    update(employee: Employee): boolean {
        return this.dao.update(employee);
    }

    delete(id: string): Employee | null {
        return this.dao.delete(id);
    }
    
}