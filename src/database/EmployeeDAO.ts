import Employee from '../models/Employee';

export default interface EmployeeDAO {
    insert(employee: Employee): boolean;
    findbyId(id: string): Employee | null;
    findAll(): Employee[] | null;
    update(employee: Employee): boolean;
    delete(id: string): Employee | null;
}