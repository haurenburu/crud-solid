import Employee from '../models/Employee';
import EmployeeDAO from './EmployeeDAO';

export default class MemoryEmployeeDAO implements EmployeeDAO {
    private employeers: Array<Employee>;

    constructor() {
        this.employeers = [];
    }

    private employeeExists(pid: string) {
        return this.employeers.find(({ id }) => pid === id ) || false
    }

    insert(employee: Employee): boolean {
        try {
            this.employeers.push(employee);
            return true;
        } catch(e) {
            return false;
        }
    }

    findbyId(id: string): Employee | null {
        const found = this.employeeExists(id);
        return found ? found : null;
    }

    findAll(): Employee[] | null {
        if(this.employeers.length > 0) {
            return this.employeers;
        }
        return null;
    }

    update(employee: Employee): boolean {
        let found = this.employeeExists(employee.id);

        if(found) {
            found = employee;
            return true;
        }

        return false;
    }

    delete(id: string): Employee | null {
        const found = this.employeeExists(id);
        if(found) {
            this.employeers = this.employeers.filter((e) => {
                return e.id !== id
            })
            return found;
        }
        return null;
    }

}