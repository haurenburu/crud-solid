import Employee from "../models/Employee";
import EmployeeDAO from "./EmployeeDAO";

export default class MySQLDAO implements EmployeeDAO {
    insert(employee: Employee): boolean {
        throw new Error("Method not implemented.");
    }
    findbyId(id: string): Employee | null {
        throw new Error("Method not implemented.");
    }
    findAll(): Employee[] | null {
        throw new Error("Method not implemented.");
    }
    update(employee: Employee): boolean {
        throw new Error("Method not implemented.");
    }
    delete(id: string): Employee | null {
        throw new Error("Method not implemented.");
    }
    
}